#!/bin/bash

alias gitaddempty='for i in $(find . -type d -empty -not -path "./.git/*"); do touch $i"/.gitignore"; done;'
